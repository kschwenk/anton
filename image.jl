# [[file:main.org::*Image][Image:3]]
# [[[[file:~/Projects/Anton/main.org::image:using_packages][image:using_packages]]][image:using_packages]]
using Colors, Images
# image:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::image:test_image_def][image:test_image_def]]][image:test_image_def]]
function test_image()
    w = 300
    h = 200
    img = zeros(RGB, h, w)
    for j = 1:w, i = 1:h
        x = (j - 0.5) / w
        y = 1.0 - (i - 0.5) / h
        img[i,j] = RGB(x, y, 0)
    end

    return img
end
# image:test_image_def ends here

img = test_image()
save("image.png", img)
# Image:3 ends here

# [[file:main.org::*Image][Image:4]]
save("image.exr", img)
# Image:4 ends here
