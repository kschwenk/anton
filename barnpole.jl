# [[file:main.org::*Barn-Pole Paradox][Barn-Pole Paradox:2]]
# [[[[file:~/Projects/Anton/main.org::transformation:using_packages][transformation:using_packages]]][transformation:using_packages]]
using Colors, Images
using LinearAlgebra, CoordinateTransformations
# transformation:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::transformation:ray_def][transformation:ray_def]]][transformation:ray_def]]
struct Ray
    position
    direction
end

function (r::Ray)(u)
    return r.position + u*r.direction
end

function (l::LinearMap)(r::Ray)
    return Ray(l(r.position), l(r.direction))
end

function (t::Translation)(r::Ray)
    return Ray(t(r.position), r.direction)
end
# transformation:ray_def ends here

# [[[[file:~/Projects/Anton/main.org::cameras:abstract_node_def][cameras:abstract_node_def]]][cameras:abstract_node_def]]
abstract type AbstractNode end

mutable struct Hit
    color
    parameter
end

function color_traversal!(hit, node::AbstractNode, ray)
end

function color(world, ray)
    hit = Hit(zero(RGB), -Inf)
    color_traversal!(hit, world, ray)
    return hit.color
end

function record_traversal!(images, node::AbstractNode, world, local_to_world)
end

function record(world)
    images = IdDict{Camera, Any}()
    record_traversal!(images, world, world, rest())
    return images
end
# cameras:abstract_node_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:transformation_utilities_def][transformation:transformation_utilities_def]]][transformation:transformation_utilities_def]]
function rest()
    return AffineMap(I, zeros(4))
end

function translate(four_vector)
    return AffineMap(I, four_vector)
end

function rotate(axis, theta)
    nx = axis[1]
    ny = axis[2]
    nz = axis[3]
    ct = cos(theta)
    st = sin(theta)
    return AffineMap(
        [nx*nx*(1-ct)+ct      nx*ny*(1-ct)-nz*st   nx*nz*(1-ct)+ny*st   0
         ny*nx*(1-ct)+nz*st   ny*ny*(1-ct)+ct      ny*nz*(1-ct)-nx*st   0
         nz*nx*(1-ct)-ny*st   nz*ny*(1-ct)+nx*st   nz*nz*(1-ct)+ct      0
         0                    0                    0                    1],
        zeros(4))
end

function boost(three_velocity)
    vx = three_velocity[1]
    vy = three_velocity[2]
    vz = three_velocity[3]
    v2 = dot(three_velocity, three_velocity)

    if v2 < 1E-14
        return rest()
    end

    gamma = 1/sqrt(1-v2)
    return AffineMap(
        [1+(gamma-1)*vx*vx/v2     (gamma-1)*vx*vy/v2     (gamma-1)*vx*vz/v2   gamma*vx
           (gamma-1)*vy*vx/v2   1+(gamma-1)*vy*vy/v2     (gamma-1)*vy*vz/v2   gamma*vy
           (gamma-1)*vz*vx/v2     (gamma-1)*vz*vy/v2   1+(gamma-1)*vz*vz/v2   gamma*vz
            gamma*vx               gamma*vy               gamma*vz            gamma   ],
        zeros(4))
end
# transformation:transformation_utilities_def ends here

# [[[[file:~/Projects/Anton/main.org::cameras:transformation_def][cameras:transformation_def]]][cameras:transformation_def]]
mutable struct Transformation <: AbstractNode
    local_to_parent
    parent_to_local

    children

    Transformation(local_to_parent, children) =
        new(local_to_parent, inv(local_to_parent), children)
end

function color_traversal!(hit, transformation::Transformation, parent_ray)
    local_ray = transformation.parent_to_local(parent_ray)

    for c in transformation.children
        color_traversal!(hit, c, local_ray)
    end

    return
end

function record_traversal!(images, transformation::Transformation, world, local_to_world)
    children_local_to_world = local_to_world ∘ transformation.local_to_parent

    for c in transformation.children
        record_traversal!(images, c, world, children_local_to_world)
    end

    return
end
# cameras:transformation_def ends here

# [[[[file:~/Projects/Anton/main.org::shade:shading_utilities_def][shade:shading_utilities_def]]][shade:shading_utilities_def]]
function debug_color(side::Int)
    if side == -1
        return RGB(0,1,1)
    elseif side == 1
        return RGB(1,0,0)
    elseif side == -2
        return RGB(1,0,1)
    elseif side == 2
        return RGB(0,1,0)
    elseif side == -3
        return RGB(1,1,0)
    elseif side == 3
        return RGB(0,0,1)
    end

    return RGB(1,1,1)
end

function debug_color(normal::AbstractVector)
    imax = argmax(abs.(normal))
    side = copysign(imax, normal[imax])
    return debug_color(side)
end

function shade(color, normal, direction)
    if color == nothing
        return debug_color(normal)
    end

    return color * (0.2 + 0.8*max(0, dot(normal, direction)))
end
# shade:shading_utilities_def ends here

# [[[[file:~/Projects/Anton/main.org::shade:sphere_def][shade:sphere_def]]][shade:sphere_def]]
mutable struct Sphere <: AbstractNode
    radius
    color
end

function color_traversal!(hit, sphere::Sphere, ray)
    o = ray.position[1:3]
    d = ray.direction[1:3]
    u_min = hit.parameter
    u_max = 0

    r = sphere.radius

    alpha = dot(d, d)
    beta = dot(o, d)
    gamma = dot(o, o) - r^2;

    discriminant = beta^2 - alpha*gamma
    if discriminant < 0
        return
    end

    sqrtdis = sqrt(discriminant)

    u_large = (-beta + sqrtdis) / alpha
    if u_min <= u_large <= u_max
        hit.parameter = u_large
        hit.color = shade(sphere.color, normalize(o + u_large * d), normalize(d))
        return
    end

    u_small = (-beta - sqrtdis) / alpha
    if u_min <= u_small <= u_max
        hit.parameter = u_small
        hit.color = shade(sphere.color, normalize(o + u_small * d), normalize(d))
        return
    end

    return
end
# shade:sphere_def ends here

# [[[[file:~/Projects/Anton/main.org::box:box_def][box:box_def]]][box:box_def]]
mutable struct Box <: AbstractNode
    half_extent
    color
end

function color_traversal!(hit, box::Box, ray)
    o = ray.position[1:3]
    d = ray.direction[1:3]
    u_min = hit.parameter
    u_max = 0

    he = box.half_extent

    side = 0
    for i = 1:3
        j = mod1(i+1,3)
        k = mod1(i+2,3)
        if d[i] != 0 # approx?
            for sign in (-1, 1)
                u = (sign*he[i] - o[i]) / d[i]
                if (u_min <= u <= u_max &&
                    abs(o[j] + u * d[j]) <= he[j] &&
                    abs(o[k] + u * d[k]) <= he[k])
                    side = sign*i
                    u_min = u
                end
            end
        end
    end

    if side != 0
        n = [i == abs(side) ? sign(side) : 0 for i = 1:3]

        hit.parameter = u_min
        hit.color = shade(box.color, n, normalize(d))
        return
    end

    return
end
# box:box_def ends here

# [[[[file:~/Projects/Anton/main.org::cameras:camera_def][cameras:camera_def]]][cameras:camera_def]]
mutable struct Camera <: AbstractNode
    vertical_field_of_view
    rolling_time
    image_size
end

function record_traversal!(images, camera::Camera, world, local_to_world)
    fov = camera.vertical_field_of_view
    T = camera.rolling_time

    iw = camera.image_size[2]
    ih = camera.image_size[1]
    id = camera.image_size[3]
    ia = iw/ih

    sz = -1
    sh = -sz * 2 * tan(fov/2)
    sw = sh*ia

    img = zeros(RGB, ih, iw, id)
    Threads.@threads for k = 1:id
        for j = 1:iw, i = 1:ih
            x = (j - 0.5) / iw
            y = 1.0 - (i - 0.5) / ih
            ct = (k - 0.5) / id

            fct = ct * T
            f = [0, 0, 0, fct]

            sx = sw * (x - 0.5)
            sy = sh * (y - 0.5)
            sct = fct - sqrt(sx^2 + sy^2 + sz^2)
            sp = [sx, sy, sz, sct]

            d = f - sp

            r = Ray(f, d)
            wr = local_to_world(r)

            img[i,j,k] = color(world, wr)
        end
    end

    images[camera] = img
    return
end

function save_moving_image(filenamebase, image)
    for k in axes(image, 3)
        filename = filenamebase * "-" * lpad(k,3,'0') * ".exr"
        save(filename, view(image, :, :, k))
    end
    save(filenamebase * ".gif", image)
end
# cameras:camera_def ends here

# [[[[file:~/Projects/Anton/main.org::barnpole:barn_pole_def][barnpole:barn_pole_def]]][barnpole:barn_pole_def]]
function barn_pole(filenamebase,
                   pole_speed,
                   barn_door_speed,
                   pole_length)
    v = [pole_speed, 0, 0]
    u = [0, barn_door_speed, 0]
    l = pole_length

    barncam = Camera(deg2rad(60), 40, (200, 300, 40))
    polecam = Camera(deg2rad(60), 40, (200, 300, 40))

    world = Transformation(rest(),
            [
                # barn
                Transformation(rest(),
                [
                    Transformation(translate([0, 0, -1.05, 0]),
                    [
                        Box([5, 5.1, 0.05], RGB(.5, .5, .5))
                    ]),
                    Transformation(translate([0, 5.05, 0, 0]),
                    [
                        Box([5, 0.05, 1.1], RGB(.5, .5, .5))
                    ]),
                    Transformation(translate([0, -5.05, 0, 0]),
                    [
                        Box([5, 0.05, 1.1], RGB(.5, .5, .5))
                    ]),
                    Transformation(translate([0, 0, 20, 0]),
                    [
                        barncam
                    ])
                ]),
                # barn doors
                Transformation(boost(u),
                [
                    Transformation(translate([-5.05, 0, 0, 0]),
                    [
                        Box([0.05, 5.1, 1.1], RGB(.75, .25, .25))
                    ]),
                    Transformation(translate([5.05, 0, 0, 0]),
                    [
                        Box([0.05, 5.1, 1.1], RGB(.75, .25, .25))
                    ])
                ]),
                # pole
                Transformation(boost(v),
                [
                    Box([l/2, 1, 1], nothing),
                    Transformation(translate([0, 0, 20, 0]),
                    [
                        polecam
                    ]),
                ])
            ])

    img = record(world)
    save_moving_image(filenamebase * "-barncam", img[barncam])
    save_moving_image(filenamebase * "-polecam", img[polecam])
    return img
end
# barnpole:barn_pole_def ends here

pole_speed = 0.9
barn_door_speed = 0.99
pole_length = 15

barn_pole("barnpole-statpole-statdoors", 0*pole_speed, 0*barn_door_speed, pole_length)
# Barn-Pole Paradox:2 ends here

# [[file:main.org::*Barn-Pole Paradox][Barn-Pole Paradox:4]]
barn_pole("barnpole-movpole-statdoors", 1*pole_speed, 0*barn_door_speed, pole_length)
# Barn-Pole Paradox:4 ends here

# [[file:main.org::*Barn-Pole Paradox][Barn-Pole Paradox:6]]
barn_pole("barnpole-movpole-movdoors", 1*pole_speed, 1*barn_door_speed, pole_length)
# Barn-Pole Paradox:6 ends here
