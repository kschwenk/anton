# [[file:main.org::*Boost][Boost:2]]
# [[[[file:~/Projects/Anton/main.org::transformation:using_packages][transformation:using_packages]]][transformation:using_packages]]
using Colors, Images
using LinearAlgebra, CoordinateTransformations
# transformation:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::transformation:ray_def][transformation:ray_def]]][transformation:ray_def]]
struct Ray
    position
    direction
end

function (r::Ray)(u)
    return r.position + u*r.direction
end

function (l::LinearMap)(r::Ray)
    return Ray(l(r.position), l(r.direction))
end

function (t::Translation)(r::Ray)
    return Ray(t(r.position), r.direction)
end
# transformation:ray_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:abstract_node_def][transformation:abstract_node_def]]][transformation:abstract_node_def]]
abstract type AbstractNode end

mutable struct Hit
    color
    parameter
end

function color_traversal!(hit, node::AbstractNode, ray)
end

function color(world, ray)
    hit = Hit(zero(RGB), -Inf)
    color_traversal!(hit, world, ray)
    return hit.color
end
# transformation:abstract_node_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:transformation_utilities_def][transformation:transformation_utilities_def]]][transformation:transformation_utilities_def]]
function rest()
    return AffineMap(I, zeros(4))
end

function translate(four_vector)
    return AffineMap(I, four_vector)
end

function rotate(axis, theta)
    nx = axis[1]
    ny = axis[2]
    nz = axis[3]
    ct = cos(theta)
    st = sin(theta)
    return AffineMap(
        [nx*nx*(1-ct)+ct      nx*ny*(1-ct)-nz*st   nx*nz*(1-ct)+ny*st   0
         ny*nx*(1-ct)+nz*st   ny*ny*(1-ct)+ct      ny*nz*(1-ct)-nx*st   0
         nz*nx*(1-ct)-ny*st   nz*ny*(1-ct)+nx*st   nz*nz*(1-ct)+ct      0
         0                    0                    0                    1],
        zeros(4))
end

function boost(three_velocity)
    vx = three_velocity[1]
    vy = three_velocity[2]
    vz = three_velocity[3]
    v2 = dot(three_velocity, three_velocity)

    if v2 < 1E-14
        return rest()
    end

    gamma = 1/sqrt(1-v2)
    return AffineMap(
        [1+(gamma-1)*vx*vx/v2     (gamma-1)*vx*vy/v2     (gamma-1)*vx*vz/v2   gamma*vx
           (gamma-1)*vy*vx/v2   1+(gamma-1)*vy*vy/v2     (gamma-1)*vy*vz/v2   gamma*vy
           (gamma-1)*vz*vx/v2     (gamma-1)*vz*vy/v2   1+(gamma-1)*vz*vz/v2   gamma*vz
            gamma*vx               gamma*vy               gamma*vz            gamma   ],
        zeros(4))
end
# transformation:transformation_utilities_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:transformation_def][transformation:transformation_def]]][transformation:transformation_def]]
struct Transformation
    children_to_parent
    parent_to_children

    children

    Transformation(children_to_parent, children) =
        new(children_to_parent, inv(children_to_parent), children)
end

function color_traversal!(hit, transformation::Transformation, parent_ray)
    children_ray = transformation.parent_to_children(parent_ray)

    for c in transformation.children
        color_traversal!(hit, c, children_ray)
    end

    return
end
# transformation:transformation_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:sphere_def][transformation:sphere_def]]][transformation:sphere_def]]
mutable struct Sphere <: AbstractNode
    radius
    color
end

function color_traversal!(hit, sphere::Sphere, ray)
    o = ray.position[1:3]
    d = ray.direction[1:3]
    u_min = hit.parameter
    u_max = 0

    r = sphere.radius

    alpha = dot(d, d)
    beta = dot(o, d)
    gamma = dot(o, o) - r^2;

    discriminant = beta^2 - alpha*gamma
    if discriminant < 0
        return
    end

    sqrtdis = sqrt(discriminant)

    u_large = (-beta + sqrtdis) / alpha
    if u_min <= u_large <= u_max
        hit.parameter = u_large
        hit.color = sphere.color
        return
    end

    u_small = (-beta - sqrtdis) / alpha
    if u_min <= u_small <= u_max
        hit.parameter = u_small
        hit.color = sphere.color
        return
    end

    return
end
# transformation:sphere_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:camera_def][transformation:camera_def]]][transformation:camera_def]]
struct Camera
    vertical_field_of_view
end

function record(camera, image_size, world)
    fov = camera.vertical_field_of_view

    iw = image_size[2]
    ih = image_size[1]
    ia = iw/ih

    sz = -1
    sh = -sz * 2 * tan(fov/2)
    sw = sh*ia

    img = zeros(RGB, ih, iw)
    for j = 1:iw, i = 1:ih
        x = (j - 0.5) / iw
        y = 1.0 - (i - 0.5) / ih

        sx = sw * (x - 0.5)
        sy = sh * (y - 0.5)
        sct = -sqrt(sx^2 + sy^2 + sz^2)
        sp = [sx, sy, sz, sct]
        f = [0, 0, 0, 0]
        d = f - sp

        r = Ray(f, d)

        img[i,j] = color(world, r)
    end

    return img
end
# transformation:camera_def ends here

# [[[[file:~/Projects/Anton/main.org::boost:boosted_spheres_def][boost:boosted_spheres_def]]][boost:boosted_spheres_def]]
function boosted_spheres(v)
    spheres = []
    for i = 0:4, j = 0:4
        s = Transformation(translate([3*i, 3*j, 0, 0]),
                [
                    Sphere(1, RGB(i/4, j/4, 1.0))
                ])
        push!(spheres, s)
    end

    world = Transformation(translate([0, 0, -20, -20]) ∘ boost(v), spheres)

    camera = Camera(deg2rad(60))
    img = record(camera, (200, 300), world)
    return img
end
# boost:boosted_spheres_def ends here

using Printf

function boosted_spheres_toward()
    speeds = 0.0 : 0.1 : 0.99
    gif = Vector{Matrix{RGB}}(undef, length(speeds))
    Threads.@threads for (i,s) in collect(enumerate(speeds))
        img = boosted_spheres([0.0, 0.0, s])
        save(@sprintf("boosted-spheres-toward-%.1f.exr", s), img)
        gif[i] = img
    end
    gif = cat(gif...; dims = 3)
    save("boosted-spheres-toward.gif", gif)
end

boosted_spheres_toward()
# Boost:2 ends here

# [[file:main.org::*Boost][Boost:4]]
function boosted_spheres_away()
    speeds = 0.0 : 0.1 : 0.99
    gif = Vector{Matrix{RGB}}(undef, length(speeds))
    Threads.@threads for (i,s) in collect(enumerate(speeds))
        img = boosted_spheres([0.0, 0.0, -s])
        save(@sprintf("boosted-spheres-away-%.1f.exr", s), img)
        gif[i] = img
    end
    gif = cat(gif...; dims = 3)
    save("boosted-spheres-away.gif", gif)
end

boosted_spheres_away()
# Boost:4 ends here

# [[file:main.org::*Boost][Boost:6]]
function boosted_spheres_sideway()
    speeds = 0.0 : 0.1 : 0.99
    gif = Vector{Matrix{RGB}}(undef, length(speeds))
    Threads.@threads for (i,s) in collect(enumerate(speeds))
        img = boosted_spheres([s, 0.0, 0.0])
        save(@sprintf("boosted-spheres-sideway-%.1f.exr", s), img)
        gif[i] = img
    end
    gif = cat(gif...; dims = 3)
    save("boosted-spheres-sideway.gif", gif)
end

boosted_spheres_sideway()
# Boost:6 ends here
