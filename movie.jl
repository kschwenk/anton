# [[file:main.org::*Movie][Movie:2]]
# [[[[file:~/Projects/Anton/main.org::transformation:using_packages][transformation:using_packages]]][transformation:using_packages]]
using Colors, Images
using LinearAlgebra, CoordinateTransformations
# transformation:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::transformation:ray_def][transformation:ray_def]]][transformation:ray_def]]
struct Ray
    position
    direction
end

function (r::Ray)(u)
    return r.position + u*r.direction
end

function (l::LinearMap)(r::Ray)
    return Ray(l(r.position), l(r.direction))
end

function (t::Translation)(r::Ray)
    return Ray(t(r.position), r.direction)
end
# transformation:ray_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:abstract_node_def][transformation:abstract_node_def]]][transformation:abstract_node_def]]
abstract type AbstractNode end

mutable struct Hit
    color
    parameter
end

function color_traversal!(hit, node::AbstractNode, ray)
end

function color(world, ray)
    hit = Hit(zero(RGB), -Inf)
    color_traversal!(hit, world, ray)
    return hit.color
end
# transformation:abstract_node_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:transformation_utilities_def][transformation:transformation_utilities_def]]][transformation:transformation_utilities_def]]
function rest()
    return AffineMap(I, zeros(4))
end

function translate(four_vector)
    return AffineMap(I, four_vector)
end

function rotate(axis, theta)
    nx = axis[1]
    ny = axis[2]
    nz = axis[3]
    ct = cos(theta)
    st = sin(theta)
    return AffineMap(
        [nx*nx*(1-ct)+ct      nx*ny*(1-ct)-nz*st   nx*nz*(1-ct)+ny*st   0
         ny*nx*(1-ct)+nz*st   ny*ny*(1-ct)+ct      ny*nz*(1-ct)-nx*st   0
         nz*nx*(1-ct)-ny*st   nz*ny*(1-ct)+nx*st   nz*nz*(1-ct)+ct      0
         0                    0                    0                    1],
        zeros(4))
end

function boost(three_velocity)
    vx = three_velocity[1]
    vy = three_velocity[2]
    vz = three_velocity[3]
    v2 = dot(three_velocity, three_velocity)

    if v2 < 1E-14
        return rest()
    end

    gamma = 1/sqrt(1-v2)
    return AffineMap(
        [1+(gamma-1)*vx*vx/v2     (gamma-1)*vx*vy/v2     (gamma-1)*vx*vz/v2   gamma*vx
           (gamma-1)*vy*vx/v2   1+(gamma-1)*vy*vy/v2     (gamma-1)*vy*vz/v2   gamma*vy
           (gamma-1)*vz*vx/v2     (gamma-1)*vz*vy/v2   1+(gamma-1)*vz*vz/v2   gamma*vz
            gamma*vx               gamma*vy               gamma*vz            gamma   ],
        zeros(4))
end
# transformation:transformation_utilities_def ends here

# [[[[file:~/Projects/Anton/main.org::transformation:transformation_def][transformation:transformation_def]]][transformation:transformation_def]]
struct Transformation
    children_to_parent
    parent_to_children

    children

    Transformation(children_to_parent, children) =
        new(children_to_parent, inv(children_to_parent), children)
end

function color_traversal!(hit, transformation::Transformation, parent_ray)
    children_ray = transformation.parent_to_children(parent_ray)

    for c in transformation.children
        color_traversal!(hit, c, children_ray)
    end

    return
end
# transformation:transformation_def ends here

# [[[[file:~/Projects/Anton/main.org::shade:shading_utilities_def][shade:shading_utilities_def]]][shade:shading_utilities_def]]
function debug_color(side::Int)
    if side == -1
        return RGB(0,1,1)
    elseif side == 1
        return RGB(1,0,0)
    elseif side == -2
        return RGB(1,0,1)
    elseif side == 2
        return RGB(0,1,0)
    elseif side == -3
        return RGB(1,1,0)
    elseif side == 3
        return RGB(0,0,1)
    end

    return RGB(1,1,1)
end

function debug_color(normal::AbstractVector)
    imax = argmax(abs.(normal))
    side = copysign(imax, normal[imax])
    return debug_color(side)
end

function shade(color, normal, direction)
    if color == nothing
        return debug_color(normal)
    end

    return color * (0.2 + 0.8*max(0, dot(normal, direction)))
end
# shade:shading_utilities_def ends here

# [[[[file:~/Projects/Anton/main.org::shade:sphere_def][shade:sphere_def]]][shade:sphere_def]]
mutable struct Sphere <: AbstractNode
    radius
    color
end

function color_traversal!(hit, sphere::Sphere, ray)
    o = ray.position[1:3]
    d = ray.direction[1:3]
    u_min = hit.parameter
    u_max = 0

    r = sphere.radius

    alpha = dot(d, d)
    beta = dot(o, d)
    gamma = dot(o, o) - r^2;

    discriminant = beta^2 - alpha*gamma
    if discriminant < 0
        return
    end

    sqrtdis = sqrt(discriminant)

    u_large = (-beta + sqrtdis) / alpha
    if u_min <= u_large <= u_max
        hit.parameter = u_large
        hit.color = shade(sphere.color, normalize(o + u_large * d), normalize(d))
        return
    end

    u_small = (-beta - sqrtdis) / alpha
    if u_min <= u_small <= u_max
        hit.parameter = u_small
        hit.color = shade(sphere.color, normalize(o + u_small * d), normalize(d))
        return
    end

    return
end
# shade:sphere_def ends here

# [[[[file:~/Projects/Anton/main.org::box:box_def][box:box_def]]][box:box_def]]
mutable struct Box <: AbstractNode
    half_extent
    color
end

function color_traversal!(hit, box::Box, ray)
    o = ray.position[1:3]
    d = ray.direction[1:3]
    u_min = hit.parameter
    u_max = 0

    he = box.half_extent

    side = 0
    for i = 1:3
        j = mod1(i+1,3)
        k = mod1(i+2,3)
        if d[i] != 0 # approx?
            for sign in (-1, 1)
                u = (sign*he[i] - o[i]) / d[i]
                if (u_min <= u <= u_max &&
                    abs(o[j] + u * d[j]) <= he[j] &&
                    abs(o[k] + u * d[k]) <= he[k])
                    side = sign*i
                    u_min = u
                end
            end
        end
    end

    if side != 0
        n = [i == abs(side) ? sign(side) : 0 for i = 1:3]

        hit.parameter = u_min
        hit.color = shade(box.color, n, normalize(d))
        return
    end

    return
end
# box:box_def ends here

# [[[[file:~/Projects/Anton/main.org::movie:camera_def][movie:camera_def]]][movie:camera_def]]
struct Camera
    vertical_field_of_view
    rolling_time
end

function record(camera, image_size, world)
    fov = camera.vertical_field_of_view
    T = camera.rolling_time

    iw = image_size[2]
    ih = image_size[1]
    id = image_size[3]
    ia = iw/ih

    sz = -1
    sh = -sz * 2 * tan(fov/2)
    sw = sh*ia

    img = zeros(RGB, ih, iw, id)
    Threads.@threads for k = 1:id
        for j = 1:iw, i = 1:ih
            x = (j - 0.5) / iw
            y = 1.0 - (i - 0.5) / ih
            ct = (k - 0.5) / id

            fct = ct * T
            f = [0, 0, 0, fct]

            sx = sw * (x - 0.5)
            sy = sh * (y - 0.5)
            sct = fct - sqrt(sx^2 + sy^2 + sz^2)
            sp = [sx, sy, sz, sct]

            d = f - sp
            r = Ray(f, d)

            img[i,j,k] = color(world, r)
        end
    end

    return img
end

function save_moving_image(filenamebase, image)
    for k in axes(image, 3)
        filename = filenamebase * "-" * lpad(k,3,'0') * ".exr"
        save(filename, image[:,:,k])
    end
    save(filenamebase * ".gif", img)
end
# movie:camera_def ends here

function flyby_box(v)
    world = Transformation(translate([0, 0, -10, 0]) ∘ boost(v),
            [
                Box([1,1,1], nothing),
            ])

    camera = Camera(deg2rad(60), 20)
    img = record(camera, (200, 300, 20), world)
    return img
end

img = flyby_box([0.9, 0.0, 0.0])
save_moving_image("movie", img)
# Movie:2 ends here
