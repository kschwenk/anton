# [[file:main.org::*Camera][Camera:2]]
# [[[[file:~/Projects/Anton/main.org::image:using_packages][image:using_packages]]][image:using_packages]]
using Colors, Images
# image:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::camera:camera_def][camera:camera_def]]][camera:camera_def]]
struct Camera
    vertical_field_of_view
end

function record(camera, image_size)
    fov = camera.vertical_field_of_view

    iw = image_size[2]
    ih = image_size[1]
    ia = iw/ih

    sz = -1
    sh = -sz * 2 * tan(fov/2)
    sw = sh * ia

    img = zeros(RGB, ih, iw)
    for j = 1:iw, i = 1:ih
        x = (j - 0.5) / iw
        y = 1.0 - (i - 0.5) / ih

        sx = sw * (x - 0.5)
        sy = sh * (y - 0.5)
        img[i,j] = RGB(sx, sy, sz)
    end

    return img
end
# camera:camera_def ends here

cam = Camera(deg2rad(60))
img = record(cam, (200, 300))
save("screen.png", clamp01.(img))
# Camera:2 ends here

# [[file:main.org::*Camera][Camera:3]]
save("screen.exr", img)
# Camera:3 ends here

# [[file:main.org::*Camera][Camera:5]]
println(img[1,1])
println(img[end,end])
# Camera:5 ends here
