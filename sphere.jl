# [[file:main.org::*Sphere][Sphere:3]]
# [[[[file:~/Projects/Anton/main.org::image:using_packages][image:using_packages]]][image:using_packages]]
using Colors, Images
# image:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::ray:ray_def][ray:ray_def]]][ray:ray_def]]
struct Ray
    position
    direction
end

function (r::Ray)(u)
    return r.position + u*r.direction
end
# ray:ray_def ends here

# [[[[file:~/Projects/Anton/main.org::sphere:sphere_def][sphere:sphere_def]]][sphere:sphere_def]]
struct Sphere
    center
    radius
    color
end

function color(sphere::Sphere, ray)
    c = sphere.center
    r = sphere.radius

    o = ray.position[1:3]
    d = ray.direction[1:3]
    u_min = -Inf
    u_max = 0

    co = o - c
    alpha = dot(d, d)
    beta = dot(co, d)
    gamma = dot(co, co) - r^2;

    discriminant = beta^2 - alpha*gamma
    if discriminant < 0
        return nothing
    end

    sqrtdis = sqrt(discriminant)

    u_large = (-beta + sqrtdis) / alpha
    if u_min <= u_large <= u_max
        return sphere.color
    end

    u_small = (-beta - sqrtdis) / alpha
    if u_min <= u_small <= u_max
        return sphere.color
    end

    return nothing
end
# sphere:sphere_def ends here

# [[[[file:~/Projects/Anton/main.org::sphere:camera_def][sphere:camera_def]]][sphere:camera_def]]
struct Camera
    vertical_field_of_view
end

function record(camera, image_size, world)
    fov = camera.vertical_field_of_view

    iw = image_size[2]
    ih = image_size[1]
    ia = iw/ih

    sz = -1
    sh = -sz * 2 * tan(fov/2)
    sw = sh*ia

    img = zeros(RGB, ih, iw)
    for j = 1:iw, i = 1:ih
        x = (j - 0.5) / iw
        y = 1.0 - (i - 0.5) / ih

        sx = sw * (x - 0.5)
        sy = sh * (y - 0.5)
        sct = -sqrt(sx^2 + sy^2 + sz^2)
        sp = [sx, sy, sz, sct]
        f = [0, 0, 0, 0]
        d = f - sp

        r = Ray(f, d)

        c = color(world, r)
        if c != nothing
            img[i,j] = c
        end
    end

    return img
end
# sphere:camera_def ends here

camera = Camera(deg2rad(60))
sphere = Sphere([0, 0, -10], 1, RGB(1, 0, 0))
img = record(camera, (200, 300), sphere)
save("sphere.png", img)
# Sphere:3 ends here
