# [[file:main.org::*Ray][Ray:3]]
# [[[[file:~/Projects/Anton/main.org::image:using_packages][image:using_packages]]][image:using_packages]]
using Colors, Images
# image:using_packages ends here

# [[[[file:~/Projects/Anton/main.org::ray:ray_def][ray:ray_def]]][ray:ray_def]]
struct Ray
    position
    direction
end

function (r::Ray)(u)
    return r.position + u*r.direction
end
# ray:ray_def ends here

# [[[[file:~/Projects/Anton/main.org::ray:camera_def][ray:camera_def]]][ray:camera_def]]
struct Camera
    vertical_field_of_view
end

function record(camera, image_size)
    fov = camera.vertical_field_of_view

    iw = image_size[2]
    ih = image_size[1]
    ia = iw/ih

    sz = -1
    sh = -sz * 2 * tan(fov/2)
    sw = sh * ia

    img = zeros(RGBA, ih, iw)
    for j = 1:iw, i = 1:ih
        x = (j - 0.5) / iw
        y = 1.0 - (i - 0.5) / ih

        sx = sw * (x - 0.5)
        sy = sh * (y - 0.5)
        sct = -sqrt(sx^2 + sy^2 + sz^2)
        sp = [sx, sy, sz, sct]
        f = [0, 0, 0, 0]
        d = f - sp

        r = Ray(f, d)

        img[i,j] = RGBA(r(-1)...)
    end

    return img
end
# ray:camera_def ends here

cam = Camera(deg2rad(60))
img = record(cam, (200, 300))
save("ray.exr", img)
println(img[1,1])
println(img[end ÷ 2, end ÷ 2])
println(img[end,end])
# Ray:3 ends here
